import { Component, Entity, World } from 'ecsy'

import EntityMultipleCache from './EntityMultipleCache'

class Id extends Component {
  id!: string

  reset(): void {
    this.id = ''
  }
}

class ExampleEntityMultipleCache extends EntityMultipleCache {
  key(entity: Entity): string | undefined {
    const component = entity.getRemovedComponent(Id) || entity.getComponent(Id)
    return component ? component.id : undefined
  }
}

describe('EntityCache', () => {
  describe('add', () => {
    it('adds entities to cache', () => {
      const world = new World()
      const cache = new ExampleEntityMultipleCache()

      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity1)

      expect(cache.items.testId).toMatchObject([entity1])

      const entity2 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity2)

      expect(cache.items.testId).toMatchObject([entity1, entity2])
    })

    it('does not add entities to cache if key is undefined', () => {
      const world = new World()
      const cache = new ExampleEntityMultipleCache()

      const entity1 = world.createEntity()
      cache.add(entity1)

      expect(Object.keys(cache.items).length).toBe(0)
    })
  })

  describe('remove', () => {
    it('removes entities from cache', () => {
      const world = new World()
      const cache = new ExampleEntityMultipleCache()
      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })
      const entity2 = world.createEntity().addComponent(Id, { id: 'testId' })

      cache.add(entity1)
      cache.add(entity2)

      entity1.removeComponent(Id)
      cache.remove(entity1)

      expect(cache.items.testId).toMatchObject([entity2])

      entity2.removeComponent(Id)
      cache.remove(entity2)

      expect(cache.items.testId.length).toBe(0)
    })

    it('does not remove entities from cache if key is undefined', () => {
      const world = new World()
      const cache = new ExampleEntityMultipleCache()

      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity1)

      cache.remove(world.createEntity())
      expect(cache.items.testId).toMatchObject([entity1])
    })
  })

  describe('get', () => {
    it('gets entity from cache', () => {
      const world = new World()
      const cache = new ExampleEntityMultipleCache()
      const entity = world.createEntity().addComponent(Id, { id: 'testId' })

      cache.add(entity)

      expect(cache.get('testId')).toMatchObject([entity])
    })

    it('returns empty array if entity with given key does not exist', () => {
      const cache = new ExampleEntityMultipleCache()

      expect(cache.get('testId').length).toBe(0)
    })
  })
})
