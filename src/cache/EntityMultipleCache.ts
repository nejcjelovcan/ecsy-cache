import { Entity } from 'ecsy'
import BaseEntityCache from './BaseEntityCache'

export default abstract class EntityMultipleCache extends BaseEntityCache<
  Entity[]
> {
  // Instance arrow function so that we can pass the reference to .forEach in Systems
  public add = (entity: Entity): void => {
    const key = this.key(entity)
    if (key) {
      if (!this.items[key]) {
        this.items[key] = []
      }
      this.items[key].push(entity)
    }
  }

  public remove = (entity: Entity): void => {
    const key = this.key(entity)
    if (key) {
      if (this.items[key]) {
        this.items[key] = this.items[key].filter(item => item !== entity)
      }
    }
  }

  public get(key: string): Entity[] {
    if (this.items[key]) {
      return this.items[key]
    }
    return []
  }
}
