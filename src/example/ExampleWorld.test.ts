import { Component, World, Entity } from 'ecsy'
import { EntitySingleCache, BaseSystem } from '../'

class Id extends Component {
  id!: string

  reset(): void {
    this.id = ''
  }
}

class Cache extends EntitySingleCache {
  key(entity: Entity): string | undefined {
    const component = entity.getComponent(Id) || entity.getRemovedComponent(Id)
    if (component) return component.id
  }
}

interface CachedWorldInterface extends World {
  cache: Cache
}

class ExampleWorld extends World implements CachedWorldInterface {
  cache: Cache

  constructor() {
    super()
    this.cache = new Cache()
  }
}

class CacheSystem extends BaseSystem<CachedWorldInterface> {
  public static queries = {
    entitiesWithId: {
      components: [Id],
      listen: {
        added: true,
        removed: true,
      },
    },
  }

  execute(): void {
    this.queries.entitiesWithId.added.forEach(this.world.cache.add)
    this.queries.entitiesWithId.removed.forEach(this.world.cache.remove)
  }
}

let world: ExampleWorld

describe('ExampleWorld', () => {
  beforeEach(() => {
    world = new ExampleWorld().registerSystem(CacheSystem)
  })

  it('adds entity to cache', () => {
    const entity = world.createEntity().addComponent(Id, { id: 'test' })
    world.execute(0, 0)

    expect(world.cache.get('test')).toMatchObject(entity)
  })

  it('removes entity from cache', () => {
    const entity = world.createEntity().addComponent(Id, { id: 'test' })
    world.execute(0, 0)

    entity.remove()
    world.execute(0, 0)

    expect(world.cache.get('test')).toBeUndefined()
  })
})
