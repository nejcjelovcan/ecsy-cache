import { Entity } from 'ecsy'
import BaseEntityCache from './BaseEntityCache'

export default abstract class EntitySingleCache extends BaseEntityCache<
  Entity
> {
  public add = (entity: Entity): void => {
    const key = this.key(entity)
    if (key) {
      this.items[key] = entity
    }
  }

  public remove = (entity: Entity): void => {
    const key = this.key(entity)
    if (key) {
      if (this.items[key] === entity) {
        delete this.items[key]
      }
    }
  }
}
