import { Component, Entity, World } from 'ecsy'

import EntitySingleCache from './EntitySingleCache'

class Id extends Component {
  id!: string

  reset(): void {
    this.id = ''
  }
}

class ExampleEntitySingleCache extends EntitySingleCache {
  key(entity: Entity): string | undefined {
    const component = entity.getRemovedComponent(Id) || entity.getComponent(Id)
    return component ? component.id : undefined
  }
}

describe('EntityMultipleCache', () => {
  describe('add', () => {
    it('adds entities to cache', () => {
      const world = new World()
      const cache = new ExampleEntitySingleCache()

      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity1)

      expect(cache.items.testId).toMatchObject(entity1)

      const entity2 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity2)

      expect(cache.items.testId).toMatchObject(entity2)
    })

    it('does not add entities to cache if key is undefined', () => {
      const world = new World()
      const cache = new ExampleEntitySingleCache()

      const entity1 = world.createEntity()
      cache.add(entity1)

      expect(Object.keys(cache.items).length).toBe(0)
    })
  })

  describe('remove', () => {
    it('removes entities from cache', () => {
      const world = new World()
      const cache = new ExampleEntitySingleCache()
      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })

      cache.add(entity1)

      entity1.removeComponent(Id)
      cache.remove(entity1)

      expect(cache.items.testId).toBeUndefined()
    })

    it('does not remove entities from cache if key is undefined', () => {
      const world = new World()
      const cache = new ExampleEntitySingleCache()

      const entity1 = world.createEntity().addComponent(Id, { id: 'testId' })
      cache.add(entity1)

      cache.remove(world.createEntity())
      expect(cache.items.testId).toMatchObject(entity1)
    })
  })

  describe('get', () => {
    it('gets entity from cache', () => {
      const world = new World()
      const cache = new ExampleEntitySingleCache()
      const entity = world.createEntity().addComponent(Id, { id: 'testId' })

      cache.add(entity)

      expect(cache.get('testId')).toMatchObject(entity)
    })

    it('returns undefined if entity with given key does not exist', () => {
      const cache = new ExampleEntitySingleCache()

      expect(cache.get('testId')).toBeUndefined()
    })
  })
})
