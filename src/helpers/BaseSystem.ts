import { System, Entity, World } from 'ecsy'

export default class BaseSystem<T extends World = World> extends System {
  public queries!: {
    [key: string]: {
      results: Entity[]
      added: Entity[]
      removed: Entity[]
      changed: Entity[]
    }
  }
  public world!: T
}
