import { Entity } from 'ecsy'

export default abstract class BaseEntityCache<T> {
  public readonly items: { [key: string]: T } = {}

  public abstract key(entity: Entity): string | undefined

  public abstract add(entity: Entity): void

  public abstract remove(entity: Entity): void

  public get(key: string): T | undefined {
    return this.items[key]
  }
}
